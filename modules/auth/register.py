from flask import Blueprint, render_template, request
from passlib.hash import bcrypt

register = Blueprint("register", __name__)

@register.route("/")
def index():
	return render_template("register/index.html")

@register.route("/register", methods = ["POST"])
def signup():
	if request.method == "POST":
		if not request.form["username"]:
			return "You forgot your username."
		else:
			username = request.form["username"]

		if not request.form["password"]:
			return "You forgot your password."
		else:
			password = request.form["password"]

		if not request.form["fname"]:
			return "You forgot a first name"
		else:
			fname = request.form["fname"]

		if not request.form["lname"]:
			return "You forgot a last name"
		else:
			ename = request.form["lname"]
		
		if not request.form["email"]:
			return "You forgot an email"
		else:
			email = request.form["email"]

		hash = bcrypt.encrypt(password, rounds = 10)

		return "Username: " + username + "<br />Password: " + hash
