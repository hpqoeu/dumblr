from flask import Blueprint, render_template, request
from passlib.hash import bcrypt

login = Blueprint("login", __name__)

@login.route("/")
def index():
	return render_template("login/index.html")

@login.route("/login", methods = ["POST"])
def auth():
	if request.method == "POST":
		if not request.form["username"]:
			return "You forgot your username."
		else:
			username = request.form["username"]

		if not request.form["password"]:
			return "You forgot your password."
		else:
			password = request.form["password"]

		hash = bcrypt.encrypt(password, rounds = 10)

		return "Username: " + username + "<br />Password: " + hash