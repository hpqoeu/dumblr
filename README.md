/usr/local/opt/openssl/bin/openssl req -x509 -newkey rsa:2048 -keyout flask.key -out flask.pem -days 365 -nodes


Flask==0.10.1
Jinja2==2.7.3
MarkupSafe==0.23
Werkzeug==0.9.6
cffi==0.8.6
cryptography==0.6
itsdangerous==0.24
passlib==1.6.2
py-bcrypt==0.4
pyOpenSSL==0.14
pycparser==2.10
six==1.8.0
