from flask import Flask, render_template
from modules.auth.login import login
from modules.auth.register import register

dumblr = Flask(__name__)
dumblr.register_blueprint(login, url_prefix="/login")
dumblr.register_blueprint(register, url_prefix="/register")

@dumblr.route("/")
def index():
	return render_template("index.html")

if __name__ == "__main__":
	dumblr.run(host='0.0.0.0', debug=True)
